FROM openjdk:18
EXPOSE 8000
ADD target/rainBirdParnas-0.0.1-SNAPSHOT.jar application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]