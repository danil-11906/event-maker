package com.rainbirdparnas.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rainbirdparnas.repository.JwtTokenRepository;
import com.rainbirdparnas.repository.UserRepository;
import com.rainbirdparnas.security.filters.JwtTokenAuthenticationFilter;
import com.rainbirdparnas.security.filters.JwtTokenAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, jsr250Enabled = true, securedEnabled = true)
public class JwtSecurityConfiguration extends WebSecurityConfigurerAdapter {

    public static final String SIGN_IN_URL = "/signIn";
    public static final String SIGN_UP_URL = "/signUp";
    public static final String SIGN_OUT_URL = "/signOut";

    @Value("${jwt.secretKey}")
    private String secretKey;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private LogoutHandler customLogoutHandler;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenRepository jwtTokenRepository;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        JwtTokenAuthenticationFilter authenticationFilter =
                new JwtTokenAuthenticationFilter(authenticationManagerBean(), objectMapper, secretKey);

        JwtTokenAuthorizationFilter authorizationFilter =
                new JwtTokenAuthorizationFilter(userDetailsService, objectMapper, secretKey, jwtTokenRepository);

        authenticationFilter.setFilterProcessesUrl(SIGN_IN_URL);

        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilter(authenticationFilter);
        http.addFilterBefore(authorizationFilter, UsernamePasswordAuthenticationFilter.class);

        http.formLogin()
                .loginPage(SIGN_IN_URL)
                .usernameParameter("email")
                .passwordParameter("password");

        http.logout().addLogoutHandler(customLogoutHandler).logoutUrl(SIGN_OUT_URL).logoutSuccessUrl("/");

        http.authorizeRequests()
                .antMatchers(SIGN_IN_URL + "/**").permitAll()
                .antMatchers(SIGN_UP_URL + "/**").permitAll()
                .antMatchers(SIGN_OUT_URL + "/**").permitAll();
    }

}

