package com.rainbirdparnas.security.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rainbirdparnas.repository.JwtTokenRepository;
import com.rainbirdparnas.security.config.JwtSecurityConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

@RequiredArgsConstructor
public class JwtTokenAuthorizationFilter extends OncePerRequestFilter {

    private final UserDetailsService userDetailsService;
    private final ObjectMapper objectMapper;
    private final String secretKey;
    private final JwtTokenRepository jwtTokenRepository;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getRequestURI().equals(JwtSecurityConfiguration.SIGN_IN_URL)
                || request.getRequestURI().equals(JwtSecurityConfiguration.SIGN_UP_URL)
                || request.getRequestURI().equals(JwtSecurityConfiguration.SIGN_OUT_URL)) {
            filterChain.doFilter(request, response);
        } else {
            String tokenHeader = request.getHeader("Authorization");

            if (tokenHeader == null) {
                logger.warn("Token is missing");
                filterChain.doFilter(request, response);
            } else if (tokenHeader.startsWith("Bearer ")) {
                String token = tokenHeader.substring("Bearer ".length());

                try {
                    if (!jwtTokenRepository.existsById(token)) {
                        DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(secretKey))
                                .build().verify(token);

                        UsernamePasswordAuthenticationToken authenticationToken =
                                new UsernamePasswordAuthenticationToken(userDetailsService
                                        .loadUserByUsername(decodedJWT.getClaim("email").asString()), null);

                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                        filterChain.doFilter(request, response);
                    } else {
                        throw new JWTVerificationException("Token expired");
                    }
                } catch (JWTVerificationException e) {
                    sendForbidden(response);
                }
            } else {
                logger.warn("Wrong token format");
                sendForbidden(response);
            }
        }
    }

    private void sendForbidden(HttpServletResponse response) throws IOException {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        objectMapper.writeValue(response.getWriter(), Collections.singletonMap("error", "User with token not found or token expired"));
    }
}
