package com.rainbirdparnas.service;

import com.rainbirdparnas.domain.dto.ControllerDTO;
import com.rainbirdparnas.domain.model.Controller;
import com.rainbirdparnas.domain.model.User;

import java.util.Set;

/**
 * @author Daniyar Zakiev
 */
public interface ControllerService {
    Controller createController(ControllerDTO controllerDTO);

    Controller deleteController(Controller controller);

    Controller findOrThrow(String manufacturedId);

    Controller updateController(ControllerDTO controllerDTO);

    void addUpdated(Controller controller, Controller.Command stationAdd);

    Boolean enableController(Controller controller);

    Controller setDateTime(Controller controller, Integer time);

    Controller addDeviceToAccount(Controller controller, User user);

    Set<Controller> findUSerControllers(Long id);
}
