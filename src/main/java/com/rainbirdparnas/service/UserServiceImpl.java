package com.rainbirdparnas.service;

import com.rainbirdparnas.domain.dto.SignUpDTO;
import com.rainbirdparnas.domain.dto.UserDTO;
import com.rainbirdparnas.domain.model.User;
import com.rainbirdparnas.exception.UserException;
import com.rainbirdparnas.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDTO signUp(SignUpDTO signUpDto) {
        if (userRepository.findUserByEmail(signUpDto.getEmail()).isEmpty()) {
            User user = User.builder()
                    .name(signUpDto.getName())
                    .email(signUpDto.getEmail())
                    .password(passwordEncoder.encode(signUpDto.getPassword()))
                    .build();

            return UserDTO.from(userRepository.save(user));
        } else {
            throw new UserException("User with email " + signUpDto.getEmail() + " already exists");
        }
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }
}
