package com.rainbirdparnas.service;

import com.rainbirdparnas.domain.dto.ManualProgramDTO;
import com.rainbirdparnas.domain.dto.ProgramDTO;
import com.rainbirdparnas.domain.dto.ProgramInfoDTO;

import java.util.List;

/**
 * @author Daniil Korotaev
 */
public interface ProgramService {
    ProgramDTO createProgram(ProgramDTO programDto, String manufacturedId);
    ProgramDTO updateProgram(ProgramDTO programDTO, Long programId, String manufacturedId);
    ProgramDTO addStationToProgram(ProgramInfoDTO programInfoDto);
    List<ProgramDTO> getAllPrograms(String controllerManufacturedId);
    void deleteById(Long id, String manufacturedId);
    ManualProgramDTO createManualProgram(ManualProgramDTO manualProgramDTO, String manufacturedId);
}
