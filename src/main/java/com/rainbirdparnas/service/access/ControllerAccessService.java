package com.rainbirdparnas.service.access;

import com.rainbirdparnas.domain.dto.AuthControllerDTO;
import com.rainbirdparnas.domain.model.Controller;
import com.rainbirdparnas.domain.model.User;
import com.rainbirdparnas.domain.model.redis.ControllerStatus;
import com.rainbirdparnas.repository.ControllerRepository;
import com.rainbirdparnas.repository.StationRepository;
import com.rainbirdparnas.repository.redis.ControllerStatusRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
@Component(value = "access")
@RequiredArgsConstructor
public class ControllerAccessService {

    @Autowired
    private ControllerRepository controllerRepository;

    @Autowired
    private ControllerStatusRepository controllerStatusRepository;

    @Autowired
    private StationRepository stationRepository;


    public boolean hasAccessToModifyController(User user, Controller controller) {
        Set<Controller> usersContaining = controllerRepository.findAllByUsersIn(List.of(user));
        return usersContaining.stream().anyMatch(c -> c.equals(controller));
    }

    public boolean hasAccessToAddController(User user, Controller controller) {
        Optional<ControllerStatus> controllerEnabled = controllerStatusRepository.findById(controller.getManufacturedId());
        return controllerEnabled.isPresent();
    }

    //ToDo придумать оптимальную проверку
    public boolean hasAccessToEnableController(AuthControllerDTO authControllerDTO) {
        return true;
    }

    public boolean hasAccessToAddStation(Controller controller) {
        Optional<ControllerStatus> controllerEnabled = controllerStatusRepository.findById(controller.getManufacturedId());
        return controllerEnabled.isPresent();
        //Также можно проверить количество станций у контроллера
    }
}
