package com.rainbirdparnas.service.util;

import com.rainbirdparnas.domain.event.StationEvent;
import com.rainbirdparnas.domain.model.Controller;
import com.rainbirdparnas.domain.model.Station;
import com.rainbirdparnas.service.ControllerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * @author Daniyar Zakiev
 */
@Service
@Slf4j
public class DomainEventListener {

    @Autowired
    private ControllerService controllerService;

    @EventListener
    public void handleStationEventAll(StationEvent event) {
        Station station = (Station) event.getObject();
        Controller controller = station.getController();

        if (event.getEvent().equals(StationEvent.Event.CREATE.getName())) {
            controllerService.addUpdated(controller, Controller.Command.STATION_ADD);
        }
        if (event.getEvent().equals(StationEvent.Event.UPDATE.getName())) {
            controllerService.addUpdated(controller, Controller.Command.STATION_UPDATE);
        }
        if (event.getEvent().equals(StationEvent.Event.DELETE.getName())) {
            controllerService.addUpdated(controller, Controller.Command.STATION_REMOVE);
        }
    }


}
