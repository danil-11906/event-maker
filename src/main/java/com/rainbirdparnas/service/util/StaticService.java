package com.rainbirdparnas.service.util;

import com.rainbirdparnas.service.ControllerService;
import org.springframework.context.ApplicationContext;

/**
 * @author Daniyar Zakiev
 */
public class StaticService {

    private static ApplicationContext applicationContext;

    public StaticService(ApplicationContext applicationContext) {
        StaticService.applicationContext = applicationContext;
    }

    private static <T> T getInstance(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    public static ControllerService getControllerService() {
        return getInstance(ControllerService.class);
    }
}
