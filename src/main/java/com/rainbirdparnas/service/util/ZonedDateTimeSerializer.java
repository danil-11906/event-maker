package com.rainbirdparnas.service.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Daniyar Zakiev
 */
public class ZonedDateTimeSerializer extends JsonSerializer<ZonedDateTime> {

    @Override
    public void serialize(ZonedDateTime dateTime, JsonGenerator generator, SerializerProvider provider)
            throws IOException {

        String dateTimeString = dateTime.format(
                DateTimeFormatter
                        .ofPattern("dd/MM/yyyy - HH:mm")
                        .withZone(dateTime.getZone()));
        generator.writeString(dateTimeString);
    }
}
