package com.rainbirdparnas.service;

import com.rainbirdparnas.domain.dto.StationDTO;
import com.rainbirdparnas.domain.embedded.StationId;
import com.rainbirdparnas.domain.model.Station;

/**
 * @author Daniyar Zakiev
 */
public interface StationService {
    Station createStation(StationDTO stationDTO);

    Station deleteStation(Station station);

    Station findOrThrow(StationId stationId);

    Station updateStation(StationDTO stationDTO);

    Station deleteStation(StationId stationId);
}
