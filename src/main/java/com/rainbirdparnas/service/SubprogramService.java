package com.rainbirdparnas.service;


import com.rainbirdparnas.domain.dto.ActiveDaysDTO;
import com.rainbirdparnas.domain.dto.ProgramDTO;

public interface SubprogramService {
    ActiveDaysDTO setEvenMode(ActiveDaysDTO activeDaysDTO);

    ActiveDaysDTO setActiveDaysWeek(ActiveDaysDTO activeDaysDTO);
    ActiveDaysDTO setActiveDaysMonth(ActiveDaysDTO activeDaysDTO);

    ProgramDTO setStartDate(ProgramDTO programDto);

    ProgramDTO setEndDate(ProgramDTO programDto);

    ActiveDaysDTO setCycleMode(ActiveDaysDTO activeDaysDTO);

    ActiveDaysDTO delay(ActiveDaysDTO activeDaysDTO);

    ProgramDTO rainSensor(ProgramDTO programDto);

    ProgramDTO freezeSensor(ProgramDTO programDto);


    ActiveDaysDTO excludeWeekDays(ActiveDaysDTO activeDaysDTO);

    ActiveDaysDTO excludeMonthDays(ActiveDaysDTO activeDaysDTO);

    ActiveDaysDTO setDaysMode(ActiveDaysDTO activeDaysDTO);
}
