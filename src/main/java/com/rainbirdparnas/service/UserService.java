package com.rainbirdparnas.service;

import com.rainbirdparnas.domain.dto.SignUpDTO;
import com.rainbirdparnas.domain.dto.UserDTO;
import com.rainbirdparnas.domain.model.User;

import java.util.Optional;

public interface UserService {
    UserDTO signUp(SignUpDTO signUpDto);

    Optional<User> findById(Long id);
}
