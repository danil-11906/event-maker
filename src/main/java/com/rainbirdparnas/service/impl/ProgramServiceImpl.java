package com.rainbirdparnas.service.impl;

import com.rainbirdparnas.domain.dto.ActiveDaysDTO;
import com.rainbirdparnas.domain.dto.ManualProgramDTO;
import com.rainbirdparnas.domain.dto.ProgramDTO;
import com.rainbirdparnas.domain.dto.ProgramInfoDTO;
import com.rainbirdparnas.domain.embedded.StationId;
import com.rainbirdparnas.domain.model.*;
import com.rainbirdparnas.exception.ErrorType;
import com.rainbirdparnas.exception.Exc;
import com.rainbirdparnas.repository.*;
import com.rainbirdparnas.service.ProgramService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Daniil Korotaev
 */
@Service
@RequiredArgsConstructor
public class ProgramServiceImpl implements ProgramService {
    private final ProgramRepository programRepository;
    private final ControllerRepository controllerRepository;
    private final StationRepository stationRepository;
    private final ProgramInfoRepository programInfoRepository;
    private final ActiveDaysRepository activeDaysRepository;
    private final ManualProgramRepository manualProgramRepository;

    @Override
    public ProgramDTO createProgram(ProgramDTO programDTO, String manufacturedId) {
        programDTO.setControllerManufacturedId(manufacturedId);
        Program program = ProgramDTO.fromDTO(programDTO);
        
        programRepository.save(program);

        setStationsToProgram(program, programDTO);
        setActiveDaysToProgram(program, programDTO);

        return ProgramDTO.toDTO(program);
    }

    @Override
    public ProgramDTO updateProgram(ProgramDTO programDTO, Long programId, String manufacturedId) {
        programDTO.setControllerManufacturedId(manufacturedId);
        Program program = programRepository.findById(programId)
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));

        Controller controller = controllerRepository.findOneByManufacturedId(programDTO.getControllerManufacturedId().toString())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND,"Controller not found"));

        program.setName(programDTO.getName());
        program.setDescription(programDTO.getDescription());
        program.setActive(programDTO.isActive());
        program.setIgnoreRain(programDTO.isIgnoreRain());
        program.setIgnoreSnow(programDTO.isIgnoreSnow());
        program.setController(controller);

        programRepository.save(program);

        return ProgramDTO.toDTO(program);
    }

    @Override
    public ProgramDTO addStationToProgram(ProgramInfoDTO programInfoDTO) {
        Program program = programRepository.findById(programInfoDTO.getProgramId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        Station station = stationRepository.findById(StationId.builder()
                .portNum(programInfoDTO.getStationPortNum())
                .controllerManufacturedId(programInfoDTO.getControllerManufacturedId())
                .build())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Station not found"));

        ProgramInfo programInfo = ProgramInfo.builder()
                .station(station)
                .periodSeconds(programInfoDTO.getPeriodSeconds())
                .build();
        programInfoRepository.save(programInfo);


        program.getProgramInfo().add(programInfo);
        programRepository.save(program);

        return ProgramDTO.toDTO(program);
    }


    private Program setStationsToProgram(Program program, ProgramDTO programDTO) {
        List<ProgramInfo> programInfoList = programDTO.getProgramInfoDTOList().stream().map(programInfoDTO -> {
            Station station = stationRepository.findById(StationId.builder()
                    .portNum(programInfoDTO.getStationPortNum())
                    .controllerManufacturedId(programDTO.getControllerManufacturedId())
                    .build())
                    .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Station not found"));

            ProgramInfo programInfo =  ProgramInfo.builder()
                    .periodSeconds(programInfoDTO.getPeriodSeconds())
                    .program(program)
                    .station(station)
                    .build();

            programInfoRepository.save(programInfo);

            return programInfo;
        }).collect(Collectors.toList());

        program.setProgramInfo(programInfoList);
        programRepository.save(program);

        return program;
    }

    private Program setActiveDaysToProgram(Program program, ProgramDTO programDTO) {
        Controller controller = controllerRepository.findOneByManufacturedId(programDTO.getControllerManufacturedId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND,"Controller not found"));

        program.setTimeStart(LocalTime.parse(programDTO.getTimeStart(), DateTimeFormatter.ofPattern("HH:mm:ss")));
        program.setActiveDateStart(ZonedDateTime.of(LocalDateTime.parse(programDTO.getActiveDateStart(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")),
                controller.getManufactured().getZone()));
        program.setActiveDateStop(ZonedDateTime.of(LocalDateTime.parse(programDTO.getActiveDateStop(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")),
                controller.getManufactured().getZone()));
        program.setController(controller);

        ActiveDays activeDays = ActiveDaysDTO.fromDTO(programDTO.getActiveDaysDTO());
        activeDays.setProgram(program);
        activeDaysRepository.save(activeDays);

        program.setActiveDays(activeDays);
        programRepository.save(program);
        return program;
    }

    @Override
    public List<ProgramDTO> getAllPrograms(String controllerManufacturedId) {
        return programRepository.findByControllerManufacturedId(controllerManufacturedId).stream()
                .map(ProgramDTO::toDTO).collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id, String manufacturedId) {
        programRepository.deleteById(id);
    }

    @Override
    public ManualProgramDTO createManualProgram(ManualProgramDTO manualProgramDTO, String manufacturedId) {
        manualProgramDTO.setControllerManufacturedId(manufacturedId);
        Controller controller = controllerRepository.findOneByManufacturedId(manualProgramDTO.getControllerManufacturedId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND,"Controller not found"));

        Program program = null;
        if(manualProgramDTO.getProgramId() != null) {
            program = programRepository.findById(manualProgramDTO.getProgramId())
                    .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        }

        Set<Station> stations = null;
        if(manualProgramDTO.getStationsPortNums() != null) {
            stations = manualProgramDTO.getStationsPortNums().stream()
                    .map(portNum -> stationRepository.findById(StationId.builder()
                            .portNum(portNum)
                            .controllerManufacturedId(manualProgramDTO.getControllerManufacturedId())
                            .build())
                            .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Station not found")))
                    .collect(Collectors.toSet());
        }

        ManualProgram manualProgram = ManualProgram.builder()
                .periodSeconds(manualProgramDTO.getPeriodSeconds())
                .isDone(false)
                .sendDate(Timestamp.from(Instant.now()))
                .controller(controller)
                .program(program)
                .stations(stations)
                .build();
        manualProgramRepository.save(manualProgram);

        return ManualProgramDTO.toDto(manualProgram);
    }
}
