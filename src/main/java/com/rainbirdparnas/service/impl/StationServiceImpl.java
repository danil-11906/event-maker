package com.rainbirdparnas.service.impl;

import com.rainbirdparnas.domain.dto.StationDTO;
import com.rainbirdparnas.domain.embedded.StationId;
import com.rainbirdparnas.domain.event.StationEvent;
import com.rainbirdparnas.domain.model.Station;
import com.rainbirdparnas.exception.ErrorType;
import com.rainbirdparnas.exception.Exc;
import com.rainbirdparnas.repository.StationRepository;
import com.rainbirdparnas.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * @author Daniyar Zakiev
 */
@Service
public class StationServiceImpl implements StationService {

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Override
    public Station createStation(StationDTO stationDTO) {
        Station station = StationDTO.to(stationDTO);
        Station saved = stationRepository.save(station);
        publisher.publishEvent((new StationEvent(Station.class.getName(), StationEvent.Event.CREATE.getName(), saved)));
        return saved;
    }

    @Override
    public Station deleteStation(Station station) {
        stationRepository.delete(station);
        publisher.publishEvent((new StationEvent(Station.class.getName(), StationEvent.Event.DELETE.getName(), station)));
        return station;
    }

    @Override
    public Station findOrThrow(StationId stationId) {
        return stationRepository.findById(stationId).orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND,"Station not found"));
    }

    @Override
    public Station updateStation(StationDTO stationDTO) {
        Station station = findOrThrow(StationDTO.getId(stationDTO));
        station.setActive(stationDTO.getActive());
        station.setName(stationDTO.getName());
        stationRepository.save(station);
        publisher.publishEvent((new StationEvent(Station.class.getName(), StationEvent.Event.UPDATE.getName(), station)));
        return station;
    }

    @Override
    public Station deleteStation(StationId stationId) {
        Station station = findOrThrow(stationId);
        return deleteStation(station);
    }


}
