package com.rainbirdparnas.service.impl;


import com.rainbirdparnas.domain.dto.ActiveDaysDTO;
import com.rainbirdparnas.domain.dto.ProgramDTO;
import com.rainbirdparnas.domain.model.ActiveDays;
import com.rainbirdparnas.domain.model.Controller;
import com.rainbirdparnas.domain.model.Program;
import com.rainbirdparnas.exception.ErrorType;
import com.rainbirdparnas.exception.Exc;
import com.rainbirdparnas.repository.ControllerRepository;
import com.rainbirdparnas.repository.ProgramRepository;
import com.rainbirdparnas.service.SubprogramService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static com.rainbirdparnas.domain.dto.ProgramDTO.toDTO;
import static com.rainbirdparnas.domain.dto.ActiveDaysDTO.toDTO;


@Service
@RequiredArgsConstructor
public class SubprogramServiceImpl implements SubprogramService {

    private final ProgramRepository programRepository;

    private final ControllerRepository controllerRepository;

    @Override
    public ActiveDaysDTO setEvenMode(ActiveDaysDTO activeDaysDTO) {
        Program program = programRepository.findById(activeDaysDTO.getProgramId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        ActiveDays activeDays = program.getActiveDays();
        activeDays.setMode(ActiveDays.Mode.EVEN);
        activeDays.setIsEven(activeDaysDTO.getIsEven());
        activeDays.setDayCycle(null);
        (program).setActiveDays(activeDays);
        return toDTO(programRepository.save(program).getActiveDays());
    }

    @Override
    public ActiveDaysDTO setActiveDaysWeek(ActiveDaysDTO activeDaysDTO) {
        Program program = programRepository.findById(activeDaysDTO.getProgramId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        ActiveDays activeDays = program.getActiveDays();
        if (activeDays.getMode() != ActiveDays.Mode.DAYS) {
            activeDays.setActiveDaysOfWeek(activeDaysDTO.getActiveDaysOfWeek());
            program.setActiveDays(activeDays);
        }
        return toDTO(programRepository.save(program).getActiveDays());
    }

    @Override
    public ActiveDaysDTO setActiveDaysMonth(ActiveDaysDTO activeDaysDTO) {
        Program program = programRepository.findById(activeDaysDTO.getProgramId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        ActiveDays activeDays = program.getActiveDays();
        activeDays.setActiveDaysOfMonth(activeDaysDTO.getActiveDaysOfMonth());
        program.setActiveDays(activeDays);
        return toDTO(programRepository.save(program).getActiveDays());
    }

    @Override
    public ProgramDTO setStartDate(ProgramDTO programDTO) {
        Program program = programRepository.findById(programDTO.getId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        Controller controller = controllerRepository.findOneByManufacturedId(programDTO.getControllerManufacturedId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Controller not found"));
        program.setActiveDateStart(ZonedDateTime.of(LocalDateTime.parse(programDTO.getActiveDateStart(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")),
                controller.getManufactured().getZone()));
        return toDTO(programRepository.save(program));
    }

    @Override
    public ProgramDTO setEndDate(ProgramDTO programDTO) {
        Program program = programRepository.findById(programDTO.getId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        Controller controller = controllerRepository.findOneByManufacturedId(programDTO.getControllerManufacturedId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Controller not found"));
        program.setActiveDateStop(ZonedDateTime.of(LocalDateTime.parse(programDTO.getActiveDateStop(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")),
                controller.getManufactured().getZone()));
        return toDTO(programRepository.save(program));
    }

    @Override
    public ActiveDaysDTO setCycleMode(ActiveDaysDTO activeDaysDTO) {
        Program program = programRepository.findById(activeDaysDTO.getProgramId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        ActiveDays activeDays = program.getActiveDays();
        activeDays.setMode(ActiveDays.Mode.CYCLE);
        activeDays.setDayCycle(activeDaysDTO.getDayCycle());
        activeDays.setIsEven(null);
        program.setActiveDays(activeDays);
        return toDTO(programRepository.save(program).getActiveDays());
    }

    @Override
    public ActiveDaysDTO delay(ActiveDaysDTO activeDaysDTO) {
        Program program = programRepository.findById(activeDaysDTO.getProgramId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        ActiveDays activeDays = program.getActiveDays();
        activeDays.setDaysRemaining(activeDaysDTO.getDaysRemaining());
        return toDTO(programRepository.save(program).getActiveDays());
    }

    @Override
    public ProgramDTO rainSensor(ProgramDTO programDTO) {
        Program program = programRepository.findById(programDTO.getId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        program.setIgnoreRain(programDTO.isIgnoreRain());
        return toDTO(programRepository.save(program));
    }

    @Override
    public ProgramDTO freezeSensor(ProgramDTO programDTO) {
        Program program = programRepository.findById(programDTO.getId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        program.setIgnoreSnow(programDTO.isIgnoreSnow());
        return toDTO(programRepository.save(program));
    }

    @Override
    public ActiveDaysDTO excludeWeekDays(ActiveDaysDTO activeDaysDTO) {
        Program program = programRepository.findById(activeDaysDTO.getProgramId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        ActiveDays activeDays = program.getActiveDays();
        if (activeDays.getMode() != ActiveDays.Mode.DAYS) {
            activeDays.setInactiveDaysOfWeek(activeDaysDTO.getInactiveDaysOfWeek());
        }
        return toDTO(programRepository.save(program).getActiveDays());
    }

    @Override
    public ActiveDaysDTO excludeMonthDays(ActiveDaysDTO activeDaysDTO) {
        Program program = programRepository.findById(activeDaysDTO.getProgramId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        ActiveDays activeDays = program.getActiveDays();
        activeDays.setInactiveDaysOfMonth(activeDaysDTO.getInactiveDaysOfMonth());
        return toDTO(programRepository.save(program).getActiveDays());
    }

    @Override
    public ActiveDaysDTO setDaysMode(ActiveDaysDTO activeDaysDTO) {
        Program program = programRepository.findById(activeDaysDTO.getProgramId())
                .orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Program not found"));
        ActiveDays activeDays = program.getActiveDays();
        activeDays.setMode(ActiveDays.Mode.DAYS);
        activeDays.setActiveDaysOfWeek(activeDaysDTO.getActiveDaysOfWeek());
        activeDays.setIsEven(null);
        activeDays.setDayCycle(null);
        activeDays.setInactiveDaysOfWeek(null);
        program.setActiveDays(activeDays);
        return toDTO(programRepository.save(program).getActiveDays());
    }
}
