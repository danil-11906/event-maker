package com.rainbirdparnas.service.impl;

import com.rainbirdparnas.domain.dto.ControllerDTO;
import com.rainbirdparnas.domain.model.Controller;
import com.rainbirdparnas.domain.model.User;
import com.rainbirdparnas.domain.model.redis.ControllerStatus;
import com.rainbirdparnas.exception.ErrorType;
import com.rainbirdparnas.exception.Exc;
import com.rainbirdparnas.repository.ControllerRepository;
import com.rainbirdparnas.repository.redis.ControllerStatusRepository;
import com.rainbirdparnas.service.ControllerService;
import com.rainbirdparnas.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author Daniyar Zakiev
 */
@Service
public class ControllerServiceImpl implements ControllerService {

    @Autowired
    private ControllerRepository controllerRepository;

    @Autowired
    private ControllerStatusRepository controllerStatusRepository;

    @Autowired
    private UserService userService;

    @Override
    public Controller createController(ControllerDTO controllerDTO) {
        Controller controller = ControllerDTO.to(controllerDTO);
        return controllerRepository.save(controller);
    }

    @Override
    public Controller deleteController(Controller controller) {
        controllerRepository.delete(controller);
        return controller;
    }

    @Override
    public Controller findOrThrow(String manufacturedId) {
        Optional<Controller> one = controllerRepository.findOneByManufacturedId(manufacturedId);
        return one.orElseThrow(Exc.sup(ErrorType.ENTITY_NOT_FOUND, "Controller not found"));
    }

    @Override
    public Controller updateController(ControllerDTO controllerDTO) {
        Controller controller = findOrThrow(controllerDTO.getManufacturedId());
        controller.setDescription(controllerDTO.getDescription());
        controller.setName(controllerDTO.getName());
        controllerRepository.save(controller);
        return controller;
    }

    @Override
    public void addUpdated(Controller controller, Controller.Command command) {
        controller.getLastCommands().add(command);
        controllerRepository.save(controller);
    }

    @Override
    public Boolean enableController(Controller controller) {
        controllerStatusRepository.save(ControllerStatus.builder()
                .manufacturedId(controller.getManufacturedId())
                .build());
        return true;
    }

    @Override
    public Controller setDateTime(Controller controller, Integer time) {
        controller.setZone(time);
        addUpdated(controller, Controller.Command.TIME_CHANGE);
        controller.setTimeZone();
        return controller;
    }

    @Override
    @Transactional
    public Controller addDeviceToAccount(Controller controller, User detachedUser) {
        User user = userService.findById(detachedUser.getId()).get();
        controller.addUser(user);
        controllerRepository.save(controller);
        return controller;
    }

    @Override
    public Set<Controller> findUSerControllers(Long id) {
        List<User> users = userService.findById(id).map(Collections::singletonList)
                .orElseGet(Collections::emptyList);
        return controllerRepository.findAllByUsersIn(users);
    }
}
