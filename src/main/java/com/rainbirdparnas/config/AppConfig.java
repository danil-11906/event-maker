package com.rainbirdparnas.config;

import com.rainbirdparnas.service.util.StaticService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
@EnableJpaRepositories(basePackages = "com.rainbirdparnas.repository"
)
public class AppConfig implements WebMvcConfigurer {

    @Bean
    public StaticService getStaticService(ApplicationContext context) {
        return new StaticService(context);
    }


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }
}
