package com.rainbirdparnas.domain.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Controller {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Id
    private String manufacturedId;
    private String name;
    private String description;
    private ZonedDateTime manufactured;
    private ZonedDateTime syncTime;
    private ZonedDateTime updateTime;
    private int zone;

    @ElementCollection(targetClass = Command.class)
    private Set<Command> lastCommands = new HashSet<>();

    @OneToMany
    private Set<Station> stations = new HashSet<>();

    @OneToMany(mappedBy = "controller")
    private List<Program> programs = new ArrayList<>();

    @OneToMany(mappedBy = "controller")
    private List<ManualProgram> manualPrograms;

    @ManyToMany
    private Set<User> users = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Controller controller = (Controller) o;
        return manufacturedId != null && Objects.equals(manufacturedId, controller.manufacturedId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @PrePersist
    public void setSyncTime() {
        this.syncTime = ZonedDateTime.now();
    }

    @PreUpdate
    public void setUpdated() {
        this.updateTime = ZonedDateTime.now();
    }

    @PostLoad
    public void setTimeZone() {
        this.syncTime = syncTime.withZoneSameInstant(ZoneOffset.ofHours(zone).normalized());
        this.updateTime = updateTime.withZoneSameInstant(ZoneOffset.ofHours(zone).normalized());
    }

    public Controller addUser(User user) {
        this.getUsers().add(user);
        return this;
    }

    public enum Command {
        STATION_ADD,
        STATION_REMOVE,
        STATION_UPDATE,
        CONTROLLER_INFO_CHANGE,
        PROGRAM_ADD,
        PROGRAM_CHANGE,
        PROGRAM_REMOVE,
        TIME_CHANGE,
    }
}


