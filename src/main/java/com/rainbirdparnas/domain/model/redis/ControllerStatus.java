package com.rainbirdparnas.domain.model.redis;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@Builder
@Data
@RedisHash(value = "ControllerStatus", timeToLive = 60 * 10 /* 10 минут */)
public class ControllerStatus {
    @Id
    private String manufacturedId;
}


