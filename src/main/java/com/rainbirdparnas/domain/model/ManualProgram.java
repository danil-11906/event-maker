package com.rainbirdparnas.domain.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

/**
 * @author Daniil Korotaev
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ManualProgram {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "period_seconds")
    private Long periodSeconds;

    @Column(name = "send_date")
    private Timestamp sendDate;

    @Column(name = "isDone")
    private Boolean isDone;

    @ManyToOne
    @JoinColumn(name = "controller")
    private Controller controller;

    @ManyToOne
    @JoinColumn(name = "program")
    private Program program;

    @ManyToMany
    @JoinTable(joinColumns = @JoinColumn(name = "manual_program_id", referencedColumnName = "id"),
            inverseJoinColumns = {
                    @JoinColumn(name = "controllerManufacturedId", referencedColumnName = "controller_id"),
                    @JoinColumn(name = "portNum", referencedColumnName = "port_name")
            })
    private Set<Station> stations;
}
