package com.rainbirdparnas.domain.model;

import lombok.*;

import javax.persistence.*;

/**
 * @author Daniil Korotaev
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Data
public class ProgramInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "period_seconds")
    private Long periodSeconds;

    @ManyToOne
    @JoinColumn(name = "program")
    private Program program;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name="controllerManufacturedId", referencedColumnName="controller_id"),
            @JoinColumn(name="portNum", referencedColumnName="port_name")
    })
    private Station station;
}
