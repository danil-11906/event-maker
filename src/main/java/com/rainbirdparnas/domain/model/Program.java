package com.rainbirdparnas.domain.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author Daniil Korotaev
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Program {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "is_active")
    private boolean isActive;
    @Column(name = "ignore_rain")
    private boolean ignoreRain;
    @Column(name = "ignore_snow")
    private boolean ignoreSnow;

    @Column(name = "time_start")
    private LocalTime timeStart;
    @Column(name = "active_date_start")
    private ZonedDateTime activeDateStart;
    @Column(name = "active_date_stop")
    private ZonedDateTime activeDateStop;

    @OneToOne(mappedBy = "program", cascade = CascadeType.REMOVE)
    private ActiveDays activeDays;

    @ManyToOne
    @JoinColumn(name = "controller")
    private Controller controller;

    @OneToMany(mappedBy = "program", cascade = CascadeType.REMOVE)
    private List<ProgramInfo> programInfo;

    @OneToMany(mappedBy = "program", cascade = CascadeType.REMOVE)
    private List<ManualProgram> manualPrograms;
}
