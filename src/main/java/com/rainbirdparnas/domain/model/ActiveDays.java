package com.rainbirdparnas.domain.model;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ActiveDays {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private Mode mode;

    @Column(name = "is_even")
    private Boolean isEven;
    @Column(name = "day_cycle")
    private Byte dayCycle;
    @Column(name = "days_remaining")
    private Byte daysRemaining;
    @Column(name = "active_days_of_week")
    private String activeDaysOfWeek;
    @Column(name = "active_days_of_month")
    private String activeDaysOfMonth;
    @Column(name = "inactive_days_of_month")
    private String inactiveDaysOfMonth;
    @Column(name = "inactive_days_of_week")
    private String inactiveDaysOfWeek;


    @OneToOne
    @JoinColumn(name = "program")
    private Program program;

    public enum Mode{
        EVEN,CYCLE,DAYS
    };
}

