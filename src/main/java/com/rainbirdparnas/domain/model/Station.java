package com.rainbirdparnas.domain.model;

import com.rainbirdparnas.domain.embedded.StationId;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Station {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idInternal;
    @EmbeddedId
    private StationId id;
    @ManyToOne
    private Controller controller;
    private String name;
    private Boolean active;

    @OneToMany(mappedBy = "station")
    private List<ProgramInfo> programInfoList;

    @ManyToMany(mappedBy = "stations")
    private Set<ManualProgram> manualPrograms;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Station station = (Station) o;
        return id != null && Objects.equals(id, station.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
