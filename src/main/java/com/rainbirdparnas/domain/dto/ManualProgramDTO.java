package com.rainbirdparnas.domain.dto;

import com.rainbirdparnas.domain.model.ManualProgram;
import com.rainbirdparnas.domain.model.Station;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Daniil Korotaev
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ManualProgramDTO {
    private Long id;
    private Long periodSeconds;
    private Timestamp sendDate;
    private Boolean isDone;
    @NotNull
    private String controllerManufacturedId;
    private Long programId;
    private List<Integer> stationsPortNums;

    public static ManualProgramDTO toDto(ManualProgram manualProgram) {
        return builder()
                .id(manualProgram.getId())
                .periodSeconds(manualProgram.getPeriodSeconds())
                .sendDate(manualProgram.getSendDate())
                .isDone(manualProgram.getIsDone())
                .controllerManufacturedId(manualProgram.getController().getManufacturedId())
                .programId(manualProgram.getProgram() == null ? null:
                        manualProgram.getProgram().getId())
                .stationsPortNums(manualProgram.getStations() == null ? null:
                        manualProgram.getStations().stream().map(station -> station.getId().getPortNum()).collect(Collectors.toList()))
                .build();
    }
}
