package com.rainbirdparnas.domain.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rainbirdparnas.domain.model.Controller;
import com.rainbirdparnas.service.util.ZonedDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * @author Daniyar Zakiev
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ControllerDTO {
    private Long id;
    @NotNull
    private String manufacturedId;
    @NotNull
    private String name;
    private String description;
    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    private ZonedDateTime manufactured;
    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    private ZonedDateTime syncTime;
    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    private ZonedDateTime updateTime;
    private int zone;

    public static ControllerDTO from(Controller controller) {
        return ControllerDTO.builder()
                .id(controller.getId())
                .updateTime(controller.getUpdateTime())
                .syncTime(controller.getSyncTime())
                .manufactured(controller.getManufactured())
                .manufacturedId(controller.getManufacturedId())
                .name(controller.getName())
                .zone(controller.getZone())
                .build();
    }

    public static Controller to(ControllerDTO controllerDTO) {
        return Controller.builder()
                .manufacturedId(controllerDTO.getManufacturedId())
                .name(controllerDTO.getName())
                .description(controllerDTO.getDescription())
                .manufactured(controllerDTO.getManufactured())
                .zone(0)
                .build();
    }
}
