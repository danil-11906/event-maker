package com.rainbirdparnas.domain.dto;

import com.rainbirdparnas.domain.embedded.StationId;
import com.rainbirdparnas.domain.model.Station;
import com.rainbirdparnas.service.util.StaticService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author Daniyar Zakiev
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StationDTO {
    private String controllerManufacturedId;
    private String name;
    @NotNull
    private Boolean active;
    @NotNull
    private Integer portNum;

    public static StationDTO from(Station station) {
        return StationDTO.builder()
                .portNum(station.getId().getPortNum())
                .controllerManufacturedId(station.getController().getManufacturedId())
                .name(station.getName())
                .active(station.getActive())
                .controllerManufacturedId(station.getId().getControllerManufacturedId())
                .build();
    }

    public static Station to(StationDTO stationDTO) {
        return Station.builder()
                .name(stationDTO.getName())
                .active(stationDTO.getActive())
                .id(getId(stationDTO))
                .controller(StaticService.getControllerService().findOrThrow(stationDTO.getControllerManufacturedId()))
                .build();
    }

    public static StationId getId(StationDTO stationDTO) {
        return StationId.builder()
                .portNum(stationDTO.getPortNum())
                .controllerManufacturedId(stationDTO.getControllerManufacturedId())
                .build();
    }
}
