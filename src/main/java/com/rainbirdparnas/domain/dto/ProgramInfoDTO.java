package com.rainbirdparnas.domain.dto;

import com.rainbirdparnas.domain.embedded.StationId;
import com.rainbirdparnas.domain.model.ProgramInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Daniil Korotaev
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProgramInfoDTO {
    private Long id;
    private Long periodSeconds;
    private Long programId;
    private String controllerManufacturedId;
    private Integer stationPortNum;

    public static ProgramInfoDTO toDTO(ProgramInfo programInfo) {
        return builder()
                .id(programInfo.getId())
                .periodSeconds(programInfo.getPeriodSeconds())
                .programId(programInfo.getProgram().getId())
                .controllerManufacturedId(programInfo.getStation().getId().getControllerManufacturedId())
                .stationPortNum(programInfo.getStation().getId().getPortNum())
                .build();
    }
}
