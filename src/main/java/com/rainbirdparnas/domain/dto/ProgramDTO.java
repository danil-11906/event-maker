package com.rainbirdparnas.domain.dto;

import com.rainbirdparnas.domain.model.Program;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Daniil Korotaev
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProgramDTO {
    private Long id;
    private String name;
    private String description;
    private ActiveDaysDTO activeDaysDTO;
    private boolean active;
    private boolean ignoreRain;
    private boolean ignoreSnow;
    private String timeStart;
    private String activeDateStart;
    private String activeDateStop;
    private List<ProgramInfoDTO> programInfoDTOList;

    private String controllerManufacturedId;

    public static ProgramDTO toDTO(Program program) {
        return builder()
                .id(program.getId())
                .name(program.getName())
                .description(program.getDescription())
                .active(program.isActive())
                .ignoreRain(program.isIgnoreRain())
                .ignoreSnow(program.isIgnoreSnow())
                .timeStart(program.getTimeStart().toString())
                .activeDateStart(program.getActiveDateStart().toString())
                .activeDateStop(program.getActiveDateStop().toString())
                .controllerManufacturedId(program.getController().getManufacturedId())
                .programInfoDTOList(program.getProgramInfo().stream().map(ProgramInfoDTO::toDTO).collect(Collectors.toList()))
                .activeDaysDTO(ActiveDaysDTO.toDTO(program.getActiveDays()))
                .build();
    }

    public static Program fromDTO(ProgramDTO programDTO) {
        return Program.builder()
                .name(programDTO.getName())
                .description(programDTO.getDescription())
                .isActive(programDTO.isActive())
                .ignoreRain(programDTO.isIgnoreRain())
                .ignoreSnow(programDTO.isIgnoreSnow())
                .build();
    }
}
