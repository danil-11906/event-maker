package com.rainbirdparnas.domain.dto;

import com.rainbirdparnas.domain.model.ActiveDays;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ActiveDaysDTO {
    private Long id;
    private String mode;
    private Boolean isEven;
    private Byte dayCycle;
    private Byte daysRemaining;
    private String activeDaysOfWeek;
    private String activeDaysOfMonth;
    private String inactiveDaysOfMonth;
    private String inactiveDaysOfWeek;
    @NotNull
    private Long programId;

    public static ActiveDaysDTO toDTO(ActiveDays activeDays){
        return builder()
                .id(activeDays.getId())
                .mode(activeDays.getMode().name())
                .isEven(activeDays.getIsEven())
                .dayCycle(activeDays.getDayCycle())
                .daysRemaining(activeDays.getDaysRemaining())
                .activeDaysOfWeek(activeDays.getActiveDaysOfWeek())
                .activeDaysOfMonth(activeDays.getActiveDaysOfMonth())
                .inactiveDaysOfWeek(activeDays.getInactiveDaysOfWeek())
                .inactiveDaysOfMonth(activeDays.getInactiveDaysOfMonth())
                .programId(activeDays.getProgram().getId())
                .build();
    }

    public static ActiveDays fromDTO(ActiveDaysDTO activeDaysDTO) {
        return ActiveDays.builder()
                .mode(ActiveDays.Mode.valueOf(activeDaysDTO.getMode()))
                .isEven(activeDaysDTO.getIsEven())
                .dayCycle(activeDaysDTO.getDayCycle())
                .daysRemaining(activeDaysDTO.getDaysRemaining())
                .activeDaysOfWeek(activeDaysDTO.getActiveDaysOfWeek())
                .activeDaysOfMonth(activeDaysDTO.getActiveDaysOfMonth())
                .inactiveDaysOfWeek(activeDaysDTO.getInactiveDaysOfWeek())
                .inactiveDaysOfMonth(activeDaysDTO.getInactiveDaysOfMonth())
                .build();
    }
}
