package com.rainbirdparnas.domain.embedded;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Daniyar Zakiev
 */
@Embeddable
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StationId implements Serializable {
    @Column(name = "controller_id")
    private String controllerManufacturedId;
    @Column(name = "port_name")
    private Integer portNum;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StationId)) return false;
        StationId that = (StationId) o;
        return Objects.equals(controllerManufacturedId, that.controllerManufacturedId) &&
                Objects.equals(portNum, that.portNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(portNum, controllerManufacturedId);
    }
}
