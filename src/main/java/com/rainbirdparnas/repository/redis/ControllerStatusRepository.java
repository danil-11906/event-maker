package com.rainbirdparnas.repository.redis;

import com.rainbirdparnas.domain.model.redis.ControllerStatus;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Daniyar Zakiev
 */
public interface ControllerStatusRepository extends CrudRepository<ControllerStatus, String> {
}
