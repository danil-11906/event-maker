package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.embedded.StationId;
import com.rainbirdparnas.domain.model.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Daniyar Zakiev
 */
@Repository
public interface StationRepository extends JpaRepository<Station, StationId> {
    Optional<Station> findByIdInternal(Long idInternal);
}
