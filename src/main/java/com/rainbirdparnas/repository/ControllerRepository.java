package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.model.Controller;
import com.rainbirdparnas.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author Daniyar Zakiev
 */
@Repository
public interface ControllerRepository extends JpaRepository<Controller, String> {
    Optional<Controller> findOneByManufacturedId(String manufacturedId);
    Set<Controller> findAllByUsersIn(List<User> users);
}
