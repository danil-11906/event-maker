package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.model.JwtToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JwtTokenRepository extends JpaRepository<JwtToken, String> {
}
