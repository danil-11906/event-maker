package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.model.District;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistrictRepository extends JpaRepository <District, Long> {
    District  findByName(String name);
}
