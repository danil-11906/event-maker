package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.model.ManualProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Daniil Korotaev
 */
@Repository
public interface ManualProgramRepository extends JpaRepository<ManualProgram, Long> {
}
