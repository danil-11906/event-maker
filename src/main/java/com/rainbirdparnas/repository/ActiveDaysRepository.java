package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.model.ActiveDays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActiveDaysRepository extends JpaRepository<ActiveDays, Long> {
}
