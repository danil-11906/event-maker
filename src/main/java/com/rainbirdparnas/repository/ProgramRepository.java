package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.model.Program;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Daniil Korotaev
 */
@Repository
public interface ProgramRepository extends JpaRepository<Program, Long> {
    List<Program> findByControllerManufacturedId(String controllerManufacturedId);
}
