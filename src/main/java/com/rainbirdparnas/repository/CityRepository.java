package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {
    List<City> findAll();
    City findByName(String name);
}
