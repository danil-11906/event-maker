package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.model.Republic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepublicRepository extends JpaRepository <Republic, Long>{
    Republic findByName(String name);
}
