package com.rainbirdparnas.repository;

import com.rainbirdparnas.domain.model.ProgramInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Daniil Korotaev
 */
@Repository
public interface ProgramInfoRepository extends JpaRepository<ProgramInfo, Long> {
}
