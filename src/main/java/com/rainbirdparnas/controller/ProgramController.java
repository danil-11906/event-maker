package com.rainbirdparnas.controller;

import com.rainbirdparnas.domain.dto.ManualProgramDTO;
import com.rainbirdparnas.domain.dto.ProgramDTO;
import com.rainbirdparnas.service.ProgramService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Daniil Korotaev
 */
@RestController
@RequestMapping(value = "/v1")
@RequiredArgsConstructor
public class ProgramController {
    private final ProgramService programService;

    @PostMapping("/controller/{manufacturedId}/program")
    public ResponseEntity<ProgramDTO> createProgram(@PathVariable(name = "manufacturedId") String manufacturedId,
                                                    @RequestBody ProgramDTO programDTO) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(programService.createProgram(programDTO, manufacturedId));
    }

    @DeleteMapping("/controller/{manufacturedId}/program/{programId}")
    public ResponseEntity<?> deleteProgram(@PathVariable(name = "manufacturedId") String manufacturedId,
                                           @PathVariable(name = "programId") Long programId) {
        programService.deleteById(programId, manufacturedId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/controller/{manufacturedId}/program/{programId}")
    public ResponseEntity<?> updateProgram(@PathVariable(name = "manufacturedId") String manufacturedId,
                                           @PathVariable(name = "programId") Long programId,
                                           @RequestBody ProgramDTO programDTO) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(programService.updateProgram(programDTO, programId, manufacturedId));
    }

    @PostMapping("/controller/{manufacturedId}/manualProgram")
    public ResponseEntity<ManualProgramDTO> createManualProgram(@PathVariable(name = "manufacturedId") String manufacturedId,
                                                                @RequestBody ManualProgramDTO manualProgramDTO) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(programService.createManualProgram(manualProgramDTO, manufacturedId));
    }

    @GetMapping("/controller/{manufacturedId}/program")
    public ResponseEntity<List<ProgramDTO>> getAllPrograms(@PathVariable(name = "manufacturedId") String manufacturedId) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(programService.getAllPrograms(manufacturedId));
    }
}
