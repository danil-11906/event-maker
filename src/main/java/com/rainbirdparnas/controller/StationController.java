package com.rainbirdparnas.controller;

import com.rainbirdparnas.domain.dto.StationDTO;
import com.rainbirdparnas.domain.embedded.StationId;
import com.rainbirdparnas.domain.model.Controller;
import com.rainbirdparnas.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author Daniyar Zakiev
 */
@RestController
@RequestMapping(value = "/v1")
public class StationController {

    @Autowired
    private StationService stationService;

    @PreAuthorize("@access.hasAccessToAddStation(#controller)")
    @PostMapping("/controller/{manufacturedId}/station")
    public StationDTO addStation(@PathVariable("manufacturedId") Controller controller, @RequestBody StationDTO stationDTO){
        stationDTO.setControllerManufacturedId(controller.getManufacturedId());
        return StationDTO.from(stationService.createStation(stationDTO));
    }

    @PreAuthorize("@access.hasAccessToAddStation(#controller)")
    @PostMapping("/controller/{manufacturedId}/station/{portNum}")
    public StationDTO deleteStation(@PathVariable("manufacturedId") Controller controller, @PathVariable("portNum") Integer stationPort){
        StationId stationId = StationId.builder()
                .portNum(stationPort)
                .controllerManufacturedId(controller.getManufacturedId())
                .build();
        return StationDTO.from(stationService.deleteStation(stationId));
    }

    @PreAuthorize("@access.hasAccessToAddStation(#controller)")
    @PutMapping("/controller/{manufacturedId}/station/{portNum}")
    public StationDTO updateStation(@PathVariable("manufacturedId") Controller controller, @RequestBody StationDTO stationDTO,
                                    @PathVariable("portNum") Integer stationPort){
        stationDTO.setPortNum(stationPort);
        stationDTO.setControllerManufacturedId(controller.getManufacturedId());
        return StationDTO.from(stationService.updateStation(stationDTO));
    }


}
