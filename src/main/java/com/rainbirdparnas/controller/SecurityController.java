package com.rainbirdparnas.controller;

import com.rainbirdparnas.domain.dto.SignUpDTO;
import com.rainbirdparnas.domain.dto.UserDTO;
import com.rainbirdparnas.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("")
public class SecurityController {
    private final UserService userService;

    @PostMapping("/signUp")
    public ResponseEntity<?> register(@RequestBody SignUpDTO signUpDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(userService.signUp(signUpDto).getId());
    }

}