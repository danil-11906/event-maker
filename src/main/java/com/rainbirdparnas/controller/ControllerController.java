package com.rainbirdparnas.controller;

import com.rainbirdparnas.domain.dto.AuthControllerDTO;
import com.rainbirdparnas.domain.dto.ControllerDTO;
import com.rainbirdparnas.domain.model.Controller;
import com.rainbirdparnas.domain.model.User;
import com.rainbirdparnas.service.ControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Daniyar Zakiev
 */
@RestController
@RequestMapping(value = "/v1")
public class ControllerController {

    @Autowired
    private ControllerService controllerService;

    //проверить, что секрет совпадает
    @PreAuthorize("@access.hasAccessToEnableController(#authControllerDTO)")
    @PostMapping("/controller/enable/{manufacturedId}")
    public Boolean enableController(@Valid @RequestBody AuthControllerDTO authControllerDTO, @PathVariable("manufacturedId") Controller controller){
        return controllerService.enableController(controller);
    }

    //проверить, что аккаунт является владельцем контроллера
    @PreAuthorize("@access.hasAccessToModifyController(#user, #controller)")
    @PutMapping("/controller/{manufacturedId}")
    public ControllerDTO setTime(@PathVariable("manufacturedId") Controller controller, @RequestBody Integer time, @AuthenticationPrincipal(expression = "user") User user){
        return ControllerDTO.from(controllerService.setDateTime(controller, time));
    }

    //проверить, что контроллер в состоянии enabled
    @PreAuthorize("@access.hasAccessToAddController(#user, #controller)")
    @PostMapping("/controller/user/{manufacturedId}")
    public ControllerDTO addDeviceToAccount(@PathVariable("manufacturedId") Controller controller, @AuthenticationPrincipal(expression = "user") User user){
        return ControllerDTO.from(controllerService.addDeviceToAccount(controller, user));
    }


    @PermitAll
    @PostMapping("/controller/init")
    public Boolean initControllers(@RequestBody(required = false) List<String> controllerManufacturedIdList){
        List<String> manufacturedIdList;
        if (controllerManufacturedIdList != null && controllerManufacturedIdList.size() > 0) {
            manufacturedIdList = controllerManufacturedIdList;
        } else {
            manufacturedIdList = generateIdList(5);
        }
        for (String id : manufacturedIdList) {
            ControllerDTO controllerDTO = ControllerDTO.builder()
                    .manufacturedId(id)
                    .name("Rain Bird X")
                    .manufactured(ZonedDateTime.now())
                    .build();
            controllerService.createController(controllerDTO);
        }
        return true;
    }

    private List<String> generateIdList(int count) {
        List<String> idList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            idList.add(UUID.randomUUID().toString());
        }
        return idList;
    }

}
