package com.rainbirdparnas.controller;

import com.rainbirdparnas.domain.dto.ActiveDaysDTO;
import com.rainbirdparnas.domain.dto.ProgramDTO;
import com.rainbirdparnas.service.SubprogramService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/subprograms")
public class SubprogramController {

    private final SubprogramService subprogramService;

    @PostMapping("/mode/even")
    public ResponseEntity<ActiveDaysDTO> setEvenMode(@RequestBody ActiveDaysDTO activeDaysDTO) {
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.setEvenMode(activeDaysDTO));

    }

    @PostMapping("/mode/days")
    public ResponseEntity<ActiveDaysDTO> setDaysMode(@RequestBody ActiveDaysDTO activeDaysDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.setDaysMode(activeDaysDTO));
    }

    @PostMapping("/active_days/month")
    public ResponseEntity<ActiveDaysDTO> setDaysModeMonth(@RequestBody ActiveDaysDTO activeDaysDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.setActiveDaysMonth(activeDaysDTO));
    }
    @PostMapping("/active_days/week")
    public ResponseEntity<ActiveDaysDTO> setDaysModeWeek(@RequestBody ActiveDaysDTO activeDaysDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.setActiveDaysWeek(activeDaysDTO));
    }

    @PostMapping("/date/start")
    public ResponseEntity<ProgramDTO> setStartDate(@RequestBody ProgramDTO programDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.setStartDate(programDTO));
    }

    @PostMapping("/date/end")
    public ResponseEntity<ProgramDTO> setEndDate(@RequestBody ProgramDTO programDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.setEndDate(programDTO));
    }

    @PostMapping("/mode/cycle")
    public ResponseEntity<ActiveDaysDTO> setCycleMode(@RequestBody ActiveDaysDTO activeDaysDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.setCycleMode(activeDaysDTO));
    }

    @PostMapping("/delay")
    public ResponseEntity<ActiveDaysDTO> setDelay(@RequestBody ActiveDaysDTO activeDaysDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.delay(activeDaysDTO));
    }

    @PostMapping("/sensor/rain")
    public ResponseEntity<ProgramDTO> adjustRainSensor(@RequestBody ProgramDTO programDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.rainSensor(programDTO));
    }

    @PostMapping("/sensor/freeze")
    public ResponseEntity<ProgramDTO> adjustFreezeSensor(@RequestBody ProgramDTO programDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.freezeSensor(programDTO));
    }

    @PostMapping("/inactive_days/week")
    public ResponseEntity<ActiveDaysDTO> excludeWeekDays(@RequestBody ActiveDaysDTO activeDaysDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.excludeWeekDays(activeDaysDTO));
    }

    @PostMapping("/inactive_days/month")
    public ResponseEntity<ActiveDaysDTO> excludeMonthDays(@RequestBody ActiveDaysDTO activeDaysDTO){
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .body(subprogramService.excludeMonthDays(activeDaysDTO));
    }
}
