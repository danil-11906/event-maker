package com.rainbirdparnas.controller;

import com.rainbirdparnas.domain.model.City;
import com.rainbirdparnas.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CityController {
    @Autowired
    CityRepository cityRepository;

    @GetMapping("/allcities")
    List<City> cityList(){
        return cityRepository.findAll();
    }
}